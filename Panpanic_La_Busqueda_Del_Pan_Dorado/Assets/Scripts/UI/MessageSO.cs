using UnityEngine;

[CreateAssetMenu(fileName = "NewMessage", menuName = "ScriptableObject/Message")]

public class MessageSO : ScriptableObject
{
    [TextArea(3, 3)]
    public string message;
}
