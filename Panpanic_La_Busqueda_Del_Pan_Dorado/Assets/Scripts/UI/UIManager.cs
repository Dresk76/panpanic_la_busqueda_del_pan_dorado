using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("SCRIPTABLE OBJECTS")]
    [SerializeField] private MessageSO messageNoWeapon;
    [SerializeField] private MessageSO messageNoKey;
    [SerializeField] private TMP_Text message;


    [Header("REFERENCES")]
    [SerializeField] private Slider slider;
    [SerializeField] private TMP_Text textCoin;
    [SerializeField] private int currentCoin = 0;




    public void ChangeMaxHealth(int healthMax)
    {
        slider.maxValue = healthMax;
    }



    public void ChangeCurrentHealth(int healthCurrent)
    {
        slider.value = healthCurrent;
    }



    public void InitializeLifeBar(int healthCurrent)
    {
        //---> Inicializar la barra de vida
        ChangeMaxHealth(healthCurrent);
        ChangeCurrentHealth(healthCurrent);
    }



    public void ChangeQuantityCoins(int coin)
    {
        currentCoin += coin;
        textCoin.text = currentCoin.ToString();
    }



    public void MessageNoWeapon()
    {
        message.text = messageNoWeapon.message;
    }



    public void MessageNoKey()
    {
        message.text = messageNoKey.message;
    }
}
