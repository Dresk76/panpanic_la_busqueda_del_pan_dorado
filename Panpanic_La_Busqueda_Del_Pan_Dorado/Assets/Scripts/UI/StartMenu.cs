using UnityEngine;

public class StartMenu : MonoBehaviour
{
    public void StartGame(string levelName)
    {
        LevelLoader.LoadLevel(levelName);
    }
}
