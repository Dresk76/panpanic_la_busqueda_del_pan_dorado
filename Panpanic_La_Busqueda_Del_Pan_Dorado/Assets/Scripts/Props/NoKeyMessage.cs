using UnityEngine;

public class NoKeyMessage : MonoBehaviour
{
    [Header("REFERENCES")]
    private Animator _animator;
    private UIManager _uIManager;



    private void Awake()
    {
        _animator = GameObject.FindGameObjectWithTag("Message").GetComponent<Animator>();
        _uIManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
    }



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            _uIManager.MessageNoKey();
            _animator.SetTrigger("FadeIn");
        }
    }



    private void OnTriggerExit(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            _animator.SetTrigger("FadeOut");
        }
    }
}
