using UnityEngine;

public class InteractProp : MonoBehaviour
{
    [Header("REFERENCES")]
    private GameManager _gameManager;
    private GameObject _weapon;
    private GameObject _key;



    private void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }


    private void Start()
    {
        _weapon = _gameManager.PropsPlayer[0];
        _key = _gameManager.PropsPlayer[1];
    }


    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            Destroy(gameObject);

            if (!_weapon.activeSelf) _weapon.SetActive(true);
            else _key.SetActive(true);
        }
    }
}
