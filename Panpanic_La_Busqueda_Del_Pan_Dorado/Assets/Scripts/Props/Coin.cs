using UnityEngine;

public class Coin : MonoBehaviour
{
    [Header("REFERENCES")]
    private UIManager _uIManager;




    private void Awake()
    {
        _uIManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
    }



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            _uIManager.ChangeQuantityCoins(1);
            Destroy(gameObject);
        }
    }
}
