using System.Collections;
using UnityEngine;

public class Chest : MonoBehaviour
{
    [Header("VARIABLES")]
    [SerializeField] private float timeToScaleWeapon = 0.5f;


    [Header("REFERENCES")]
    [SerializeField] private Animator animatorChest;
    [SerializeField] private Animator animatorProp;



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            StartCoroutine(LogicAnimation());
        }
    }



    private IEnumerator LogicAnimation()
    {
        animatorChest.SetTrigger("Open");
        yield return new WaitForSeconds(timeToScaleWeapon);
        animatorProp.SetTrigger("Scale");
        gameObject.GetComponent<BoxCollider>().enabled = false;
    }
}
