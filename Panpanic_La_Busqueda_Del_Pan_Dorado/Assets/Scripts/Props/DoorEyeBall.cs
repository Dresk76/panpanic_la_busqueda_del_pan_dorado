using UnityEngine;

public class DoorEyeBall : MonoBehaviour
{
    [Header("REFERENCES")]
    [SerializeField] private BoxCollider areaMessage;
    private GameManager _gameManager;
    private GameObject _weapon;
    private Animator _animator;



    private void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _animator = GetComponentInChildren<Animator>();
    }



    private void Start()
    {
        _weapon = _gameManager.PropsPlayer[0];
    }



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player") && _weapon.activeSelf)
        {
            _animator.SetTrigger("OpenDoor");
            areaMessage.enabled = false;

            BoxCollider[] boxColliders = GetComponents<BoxCollider>();

            foreach (BoxCollider boxCollider in boxColliders)
            {
                boxCollider.enabled = false;
            }
        }
    }
}
