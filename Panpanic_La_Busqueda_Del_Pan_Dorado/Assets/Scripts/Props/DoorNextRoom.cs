using UnityEngine;

public class DoorNextRoom : MonoBehaviour
{
    [Header("ENEMIES")]
    [SerializeField] private int numberEnemies;
    [SerializeField] private int enemiesDestroyed;


    [Header("REFERENCES")]
    private Animator _animator;
    private BoxCollider _boxCollider;




    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
        _boxCollider = GetComponentInChildren<BoxCollider>();
    }



    private void Start()
    {
        numberEnemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
    }



    private void OpenDoor()
    {
        _animator.SetTrigger("OpenDoor");
        _boxCollider.enabled = false;
    }



    public void AddDestroyedEnemy()
    {
        enemiesDestroyed += 1;

        if (enemiesDestroyed == numberEnemies)
        {
            OpenDoor();
        }
    }
}
