using UnityEngine;

public class DropItem : MonoBehaviour
{
    [Header("DROP ITEMS")]
    [SerializeField] private GameObject[] items;
    [Range(0f, 1f)]
    [SerializeField] private float probability = 0.7f;




    public void DropearItem(GameObject positionDrop)
    {
        int dropProbability;

        if (Random.value < probability) dropProbability = 0;
        else dropProbability = 1;

        Instantiate(items[dropProbability], positionDrop.transform.position + Vector3.right * Random.Range(0f, 4f) + Vector3.up * 0.6f, Quaternion.identity);
    }
}
