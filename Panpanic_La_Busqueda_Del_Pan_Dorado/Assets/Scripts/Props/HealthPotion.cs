using UnityEngine;

public class HealthPotion : MonoBehaviour
{
    [Header("HEALTH")]
    private int _newLife = 10;


    [Header("REFERENCES")]
    private GameManager _gameManager;




    private void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            _gameManager.GetHealth(_newLife);
            Destroy(gameObject);
        }
    }
}
