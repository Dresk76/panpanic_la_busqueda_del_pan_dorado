using UnityEngine;

public class EnemiesArea : MonoBehaviour
{
    //--> Properties
    public bool CanMove { get; set; }



    private void OnTriggerEnter(Collider colliderr)
    {
        colliderr.CompareTag("Player");
        {
            CanMove = true;
            Destroy(gameObject);
        }
    }
}
