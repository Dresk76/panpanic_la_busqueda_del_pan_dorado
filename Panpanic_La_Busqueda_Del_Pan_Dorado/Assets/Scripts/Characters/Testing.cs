using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            gameManager.GetDamage();
        }


        if (Input.GetKeyDown(KeyCode.R))
        {
            gameManager.GetHealth(10);
        }
    }
}
