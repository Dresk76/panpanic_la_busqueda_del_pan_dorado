using UnityEngine;
using UnityEngine.AI;

public abstract class Enemy : MonoBehaviour
{
    [Header("HEALTH")]
    [SerializeField] protected int healthMax;
    [SerializeField] protected int healthCurrent;
    [SerializeField] private int weaponDamage;


    [Header("MOVEMENT")]
    [SerializeField] protected GameObject destinationPlayer;
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private Animator animator;


    [Header("SPHERE CAST")]
    [SerializeField] protected float sphereRadius;
    [SerializeField] protected float maxDistance;
    [SerializeField] protected LayerMask layerMask;
    [SerializeField] protected Transform origin;
    private Vector3 direction;
    public bool PlayerInAttackingZone { get; set; }


    [Header("ATTACK")]
    [SerializeField] protected int damage;
    [SerializeField] private float timeToAttack;
    [SerializeField] private float nextAttackTime;


    //--> Getters And Setters
    public int WeaponDamage { get => weaponDamage; set => weaponDamage = value; }
    public int HealthCurrent { get => healthCurrent; set => healthCurrent = value; }
    public float TimeToAttack { get => timeToAttack; }
    public float NextAttackTime { get => nextAttackTime; set => nextAttackTime = value; }
    public GameObject DestinationPlayer { get => destinationPlayer; set => destinationPlayer = value; }
    public Animator MyAnimator { get => animator; set => animator = value; }


    private void Start()
    {
        healthCurrent = healthMax;
        direction = transform.forward;
    }



    public void SphereCaster()
    {
        PlayerInAttackingZone = Physics.SphereCast(origin.position, sphereRadius, direction, out RaycastHit hitInfo, maxDistance, layerMask);
    }



    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin.position, origin.position + transform.forward * maxDistance);
        Gizmos.DrawWireSphere(origin.position + transform.forward * maxDistance, sphereRadius);
    }



    public void Move()
    {
        navMeshAgent.SetDestination(destinationPlayer.transform.position);
    }


    public void Stop()
    {
        navMeshAgent.velocity = Vector3.zero;
        animator.SetFloat("Speed", 0f);
    }



    public void MoveAnimation()
    {
        animator.SetFloat("Speed", navMeshAgent.velocity.magnitude / navMeshAgent.speed);
    }



    public void TimeBetweenAttacks()
    {
        if (nextAttackTime > 0)
        {
            nextAttackTime -= Time.deltaTime;
        }
    }



    public virtual void BasicAttack()
    {
        if (PlayerInAttackingZone && nextAttackTime <= 0)
        {
            Debug.Log("Basic Attack");
        }
    }



    public void GetDamage(int damage)
    {
        if (healthCurrent <= 0) return;

        healthCurrent -= damage;

        if (healthCurrent <= 0) Death();
        else Hit();
    }



    public virtual void Hit()
    {
        Debug.Log("Golpe");
    }



    public virtual void Death()
    {
        Debug.Log("Murio");
    }
}
