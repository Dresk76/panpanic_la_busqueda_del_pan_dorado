using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public int Health { get; set; }


    [Header("MOVEMENT")]
    [SerializeField] private float _moveSpeed = 3f;
    [SerializeField] private float rotationSmooth = 0.2f;
    private Vector2 _input;
    private Vector3 _camForward, _camRight;
    private Vector3 _moveAxis, _moveDir, _movement;
    private float _movementSpeed;
    private bool _canMove = true;
    private bool _isDead;


    [Header("JUMP")]
    [SerializeField] private float gravity = 30f;
    [SerializeField] private float fallVelocity = 0f;
    [SerializeField] private float jumpForce = 9f;
    private bool _canJump;
    private bool _isGround = true;


    [Header("REFERENCES")]
    [SerializeField] private GameManager gameManager;
    private CharacterController _characterController;
    private Transform _camera;
    private PlayerInput _playerInput;
    private Animator _animator;


    [Header("ATTACK")]
    [SerializeField] private float timeToAttack = 1f;
    [SerializeField] private float nextAttackTime;



    //--> Getters And Setters
    public bool IsDead { get => _isDead; set => _isDead = value; }





    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _camera = Camera.main.transform;
        _playerInput = GetComponent<PlayerInput>();
        _animator = GetComponentInChildren<Animator>();
    }



    private void Update()
    {
        if (_camera != null)
        {
            Move();
            LateUpdate();
            TimeBetweenAttacks();
        }
    }



    private void Move()
    {
        _input = _playerInput.actions["Move"].ReadValue<Vector2>();

        _moveAxis = new Vector3(_input.x, 0f, _input.y);
        _movementSpeed = _moveAxis.magnitude;

        if (_moveAxis.magnitude > 1)
            _moveAxis = _moveAxis.normalized * _moveSpeed;
        else
            _moveAxis *= _moveSpeed;


        CameraDirection();
        _moveDir = _moveAxis.x * _camRight + _moveAxis.z * _camForward;

        if (_input.x != 0 || _input.y != 0)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_moveDir), rotationSmooth);
        }

        SetGravity();
        SetJump();

        if (!_canMove || _isDead)
        {
            _movement = Vector3.zero;
        }
        else
        {
            _movement = _moveDir * Time.deltaTime;
        }

        _characterController.Move(_movement);
    }



    private void CameraDirection()
    {
        _camForward = _camera.forward.normalized;
        _camForward.y = 0;

        _camRight = _camera.right.normalized;
        _camRight.y = 0;
    }



    //--> Si esta tocando el suelo asignar un valor a fallVelocity, para que el player siempre este siendo empujado hacia abajo sino en cada frame restarle el valor gravedad
    private void SetGravity()
    {
        if (_characterController.isGrounded)
        {
            _isGround = true;
            fallVelocity = -gravity * Time.deltaTime;
        }
        else
        {
            _isGround = false;
            _canJump = false;
            fallVelocity -= gravity * Time.deltaTime;
        }

        _moveDir.y = fallVelocity;
    }



    private void SetJump()
    {
        if (_characterController.isGrounded && _canJump)
        {
            fallVelocity = jumpForce;
            _moveDir.y = fallVelocity;
        }
    }



    public void Jump(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            _canJump = true;
        }
    }



    public void FirstAttack(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed && nextAttackTime <= 0)
        {
            _animator.SetTrigger("FirstAttack");
            nextAttackTime = timeToAttack;
        }
    }



    private void TimeBetweenAttacks()
    {
        if (nextAttackTime > 0)
        {
            nextAttackTime -= Time.deltaTime;
        }
    }



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("EyeBallAttack") || collider.CompareTag("CoyongoAttack"))
        {
            gameManager.GetDamage();
        }
    }



    private void LateUpdate()
    {
        //--> Movement
        _animator.SetFloat("velX", _moveAxis.x);
        _animator.SetFloat("velZ", _moveAxis.z);
        _animator.SetFloat("Speed", _movementSpeed);

        //--> Jump
        _animator.SetFloat("Jump", fallVelocity);
        _animator.SetBool("IsGrounded", _isGround);


        //--> Attack
        if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("FirstAttack"))
            _canMove = false;
        else
            _canMove = true;


        //--> Death
        if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("MarialeDeath"))
            _isDead = true;
        else
            _isDead = false;
    }
}
