using System.Collections;
using UnityEngine;

public class Coyongo : Enemy
{
    [Header("MOVEMENT")]
    [SerializeField] private bool _canMove = true;
    [SerializeField] private bool _isDead;
    [SerializeField] private float _timeMove = 2f;


    [Header("REFERENCES")]
    [SerializeField] private EnemiesArea enemiesArea;
    [SerializeField] private DoorNextRoom doorNextRoom;
    private PlayerController _playerController;
    private DropItem _dropItem;
    private ParticleSystem _particleSystem;





    private void Awake()
    {
        DestinationPlayer = GameObject.FindGameObjectWithTag("Player");
        _playerController = DestinationPlayer.GetComponent<PlayerController>();
        _dropItem = GameObject.FindGameObjectWithTag("DropItem").GetComponent<DropItem>();
        _particleSystem = GetComponentInChildren<ParticleSystem>();
    }



    private void Update()
    {
        //--> Movemennt
        if (enemiesArea.CanMove && _canMove && !_isDead && !_playerController.IsDead)
        {
            Move();
            MoveAnimation();
        }

        if (!_canMove)
        {
            Stop();
        }


        //--> Attack
        if (!_isDead && !_playerController.IsDead)
        {
            TimeBetweenAttacks();
            SphereCaster();
            BasicAttack();
        }
    }



    public override void BasicAttack()
    {
        if (PlayerInAttackingZone && NextAttackTime <= 0)
        {
            StartCoroutine(CoyongoAttack());
        }
    }



    private IEnumerator CoyongoAttack()
    {
        _canMove = false;
        NextAttackTime = TimeToAttack;
        MyAnimator.SetTrigger("Attack");

        yield return new WaitForSeconds(_timeMove);

        _canMove = true;
    }



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Weapon"))
        {
            GetDamage(WeaponDamage);
        }
    }


    public override void Hit()
    {
        MyAnimator.SetTrigger("Damage");
    }



    public override void Death()
    {
        StartCoroutine(BeforeDeath());
    }



    private IEnumerator BeforeDeath()
    {
        _particleSystem.Play();
        MyAnimator.SetTrigger("Death");
        doorNextRoom.AddDestroyedEnemy();
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
        _dropItem.DropearItem(gameObject);
        _dropItem.DropearItem(gameObject);
        _dropItem.DropearItem(gameObject);
    }



    private void LateUpdate()
    {
        //--> Death
        if (MyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("CoyongoDeath"))
            _isDead = true;
        else
            _isDead = false;
    }
}
