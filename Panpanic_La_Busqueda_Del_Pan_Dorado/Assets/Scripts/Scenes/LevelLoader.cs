using UnityEngine.SceneManagement;

public static class LevelLoader
{

    public static string NextLevel { get; set; }


    public static void LoadLevel(string levelName)
    {
        //--> Inicializar la propiedad con la escena a cargar
        NextLevel = levelName;
        SceneManager.LoadScene("Loader");
    }
}
