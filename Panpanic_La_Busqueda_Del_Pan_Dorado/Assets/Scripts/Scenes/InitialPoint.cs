using UnityEngine;

public class InitialPoint : MonoBehaviour
{
    [Header("REFERENCES")]
    private GameObject _player;
    private Transform _initialPoint;




    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _initialPoint = GetComponent<Transform>();
    }



    private void Start()
    {
        MoveToInitialPoint();
    }



    private void MoveToInitialPoint()
    {
        _player.transform.position = _initialPoint.position;
    }
}
