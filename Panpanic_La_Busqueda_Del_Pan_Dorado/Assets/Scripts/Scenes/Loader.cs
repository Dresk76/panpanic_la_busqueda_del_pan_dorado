using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    [Header("VARIABLES")]
    [SerializeField] private string levelToLoad;



    private void Start()
    {
        levelToLoad = LevelLoader.NextLevel;
        StartCoroutine(LoadScene(levelToLoad));
    }



    public IEnumerator LoadScene(string levelName)
    {
        // Solo para ver la animacion
        // yield return new WaitForSeconds(3f);

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(levelName);

        while (!asyncOperation.isDone)
        {
            yield return null;
        }
    }
}
