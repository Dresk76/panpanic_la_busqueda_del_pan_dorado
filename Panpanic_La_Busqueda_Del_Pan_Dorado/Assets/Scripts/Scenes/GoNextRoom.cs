using UnityEngine;

public class GoNextRoom : MonoBehaviour
{
    [SerializeField] private string nextScene;



    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            Destroy(gameObject);
            LevelLoader.LoadLevel(nextScene);
        }
    }
}
