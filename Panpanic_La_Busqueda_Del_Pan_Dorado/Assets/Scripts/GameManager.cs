using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("HEALTH")]
    [SerializeField] private int healthMax = 100;
    [SerializeField] private int healthCurrent;


    [Header("DAMAGE")]
    [SerializeField] private int eyeBallDamage = 10;


    [Header("REFERENCES")]
    [SerializeField] private Animator animatorPlayer;
    [SerializeField] private UIManager uIManager;


    [Header("PROPS")]
    [SerializeField] private GameObject[] propsPlayer;


    //--> Getters and Setters
    public GameObject[] PropsPlayer { get => propsPlayer; set => propsPlayer = value; }



    private void Start()
    {
        healthCurrent = healthMax;

        uIManager.InitializeLifeBar(healthCurrent);
    }




    public void GetHealth(int health)
    {
        if (healthCurrent == healthMax) return;

        healthCurrent += health;

        uIManager.ChangeCurrentHealth(healthCurrent);
    }



    public void GetDamage()
    {
        if (healthCurrent <= 0) return;
        
        healthCurrent -= eyeBallDamage;

        if (healthCurrent <= 0) Die();
        else Hit();

        uIManager.ChangeCurrentHealth(healthCurrent);
    }



    private void Hit()
    {
        animatorPlayer.SetTrigger("Damage");
    }



    private void Die()
    {
        animatorPlayer.SetTrigger("Death");
    }
}
